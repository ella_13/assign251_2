package assign251_2;

import org.apache.log4j.*;

public class MemAppender {
	final static Logger logger = Logger.getLogger("MemAppender");
	
	
	public static void main(String[] args) throws Exception {
		BasicConfigurator.configure();
	
		Thread.currentThread();
		Thread.sleep(100);
		
		logger.debug("debug message");
		logger.warn("warn message");
		logger.error("error message");
		
		Thread.currentThread();
		Thread.sleep(100);
		LogManager.shutdown();
		
	}
	
	public void setLayout() {
		// probably calls VelocityLayout
	}
	
	public void getCurrentLogs() {
		// returns an unmodifiable list of the LoggingEvents
	}
	
	public void getEventStrings() {
		// returns an unmodifiable list of strings (using the layout)
	}
	
	public void printLogs() {
		// prints the logging events to the console using the layout, and then clears the logs from its memory
	}
}
